import 'dart:convert';
import 'dart:io';

import 'package:path_provider/path_provider.dart';

Future<File> _getFile() async {
  final directory = await getApplicationDocumentsDirectory();
  return File("${directory.path}/game_save.json");
}

Future<File> saveData(gameData) async {
  String data = json.encode(gameData);
  final file = await _getFile();
  return file.writeAsString(data);
}

Future<String> readData() async {
  try {
    final file = await _getFile();

    return file.readAsString();
  } catch(e){
    return null;
  }
}