library constants;
import 'package:flutter/material.dart';

const Map<String, Color> THEME_COLORS = {
  "Receitas": Colors.green,
  "income": Colors.green,
  "Gastos": Colors.red,
  "expense": Colors.red,
  "Dívidas": Colors.cyan,
  "debt": Colors.orange,
  "Ativos": Colors.purple,
  "stock": Colors.purple,
  "Pagamento": Colors.orange,
  "Pagar": Colors.red,
  "Receber": Colors.green,
  "default": Colors.orange,
  "APPL3": Color.fromARGB(255, 59, 64, 152),
  "VAL3": Color.fromARGB(255, 141, 191, 97),
  "BB4S": Color.fromARGB(255, 237, 47, 90),
  "AMZ4": Color.fromARGB(255, 242, 129, 51),
};

const Map<String, IconData> THEME_ICONS = {
  "Receitas": Icons.add,
  "income": Icons.add,
  "Gastos": Icons.remove,
  "expense": Icons.remove,
  "Dívidas": Icons.error,
  "debt": Icons.error,
  "Ativos": Icons.show_chart,
  "stock": Icons.show_chart,
  "Pagamento": Icons.attach_money,
  "Pagar": Icons.remove_circle_outline,
  "Receber": Icons.add_circle_outline,
  "default": Icons.attach_money,
};

const Map<String, int> STOCK_POSITIONS = {
  "APPL3": 0,
  "VAL3": 1,
  "BB4S": 2,
  "AMZ4": 3,
};

const List<String> CHARACTERS = ["Alex", "Márcia", "Bruno", "Eduardo", "Jorge", "Leila"];