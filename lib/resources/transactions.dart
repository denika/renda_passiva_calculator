import 'package:flutter/material.dart';

import '../constants.dart';

class Transaction {
  String title;
  String type;
  double value;
  double total;
  bool isDismissible;
  bool isChecked;

  Transaction(String type, {String title, double value, bool isDismissible=true, double total, bool isChecked=false}){
    this.type = type;
    this.title = title;
    this.value = value;
    if(type=="debt" && total!=null) this.total = total;
    this.isDismissible = isDismissible;
    this.isChecked = isChecked;
  }

  IconData defaultIcon(){
    return THEME_ICONS[type];
  }

  Color defaultColor(){
    return THEME_COLORS[type];
  }

  Color titleColor([Color _defaultColor]){
    if(!isDebt() && isDismissible) return Colors.black;

    return isChecked ? Colors.grey : (_defaultColor ?? defaultColor());
  }

  bool isIncome(){
    return type == "income";
  }

  bool isDebt(){
    return type == "debt";
  }

  bool isExpense(){
    return type == "expense";
  }

  bool isStock(){
    return type == "stock";
  }

  void halveValue(){
    if(isStock() && value >= 2){
      value = (value/2).floor().toDouble();
    }
  }

  void doubleValue(){
    if(isStock()){
      value = value*2;
    }
  }

  void addStock(int quantity){
    if(isStock()){
      value += quantity;
    }
  }

  void removeStock(int quantity) {
    if(isStock()){
      value -= quantity;
    }
  }

  String toString(){
    return "$type - $title - $value - $total";
  }
}