import 'package:renda_passiva_calculator/resources/transactions.dart';

import '../constants.dart';

class Character {
  String _name;
  String _job;
  List<Transaction> _incomes = List();
  List<Transaction> _expenses = List();
  List<Transaction> _debts = List();
  List<Transaction> _stocks = List();
  double _balance;
  double _salary;

  Character(String name, {Map<String, dynamic> baseData}){
    this._name = name;

    Map<String, dynamic> data = baseData == null ? _charactersData[name] : baseData[name];

    this._job = data["job"];
    this._salary = data["salary"];

    this._incomes.add(Transaction("income",
        title: "Salário",
        value: data["salary"],
        isDismissible: false
    ));
    data["incomes"].forEach((k, v){
      this._incomes.add(Transaction("income",
          title: k,
          value: v.toDouble()
      ));
    });

    data["expenses"]["initial"].forEach((k, v){
      this._expenses.add(Transaction("expense",
          title: k,
          value: v.toDouble(),
          isDismissible: false
      ));
    });
    data["expenses"]["current"].forEach((k, v){
      this._expenses.add(Transaction("expense",
          title: k,
          value: v.toDouble(),
          isDismissible: k!="Filho"
      ));
    });

    data["debts"]["initial"].forEach((k, v){
      this._debts.add(Transaction("debt",
          title: k,
          value: v["expenses"].toDouble(),
          total: v["total"].toDouble(),
          isChecked: v["checked"] ?? false,
          isDismissible: false
      ));
    });
    data["debts"]["current"].forEach((k, v){
      this._debts.add(Transaction("debt",
          title: k,
          value: v["expenses"].toDouble(),
          total: v["total"].toDouble(),
          isChecked: v["checked"]
      ));
    });

    if(data.containsKey("stocks")){
      data["stocks"].forEach((k, v){
        this._stocks.add(Transaction("stock",
            title: k, value: v, total: 0, isDismissible: true
        ));
      });
    }

    _balance = data["balance"] ?? 0;

    if(baseData == null){
      receivePayment();

      STOCK_POSITIONS.keys.toList().forEach((element) {
        this._stocks.add(Transaction("stock",
          title: element, value: 0, total: 0, isDismissible: true
        ));
      });
    }
  }

  /* GETTERS */
  String get name => _name;
  String get job => _job;
  double get salary => _salary;
  double get balance => _balance;
  List<Transaction> get incomes => _incomes;
  List<Transaction> get expenses => _expenses;
  List<Transaction> get debts => _debts;
  List<Transaction> get stocks => _stocks;

  double totalIncomes(){
    double _total = 0.0;
    for(Transaction income in _incomes){
      _total += income.value;
    }
    return _total;
  }

  double totalExpenses(){
    double _total = 0.0;
    for(Transaction expense in _expenses){
      _total += expense.value;
    }
    for(Transaction debt in _debts){
      if(!debt.isChecked){
        _total += debt.value;
      }
    }
    return _total;
  }

  double payment(){
    return totalIncomes() - totalExpenses();
  }

  double passiveIncome(){
    double _total = 0.0;

    for(Transaction income in _incomes){
      if(["Negócios", "Imóvel", "Investimento"].contains(income.title)) {
        _total += income.value;
      }
    }

    return _total;
  }

  void receivePayment(){
    _balance += payment();
  }

  void receive(double value){
    _balance += value;
  }

  bool pay(double value){
    if(value == null) return false;
    if(_balance < value) return false;

    _balance -= value;
    return true;
  }

  bool addIncome(Transaction transaction){
    if(transaction.total != null){
      if(_balance < transaction.total) return false;

      _balance -= transaction.total;
    }
    _incomes.add(transaction);

    return true;
  }

  bool addExpense(Transaction transaction){
    _expenses.add(transaction);
    return true;
  }

  bool addDebt(Transaction transaction){
    _debts.add(transaction);
    return true;
  }

  bool buyStock(Transaction transaction){
    if(transaction.value == null || transaction.value == 0) return true;

    double _payTotal = transaction.value * transaction.total;

    if(!pay(_payTotal)) return false;

    Transaction _stock = _stocks.elementAt(STOCK_POSITIONS[transaction.title]);
    _stock.addStock(transaction.value.floor());
    return true;
  }

  bool sellStock(Transaction transaction){
    Transaction _stock = _stocks.removeAt(STOCK_POSITIONS[transaction.title]);

    if(transaction.value == null || transaction.value == 0) {
      returnStock(_stock);
      return false;
    }

    receive(transaction.value * transaction.total);

    _stock.removeStock(transaction.value.floor());
    returnStock(_stock);

    return true;
  }

  void returnStock(Transaction transaction){
    _stocks.insert(STOCK_POSITIONS[transaction.title], transaction);
  }

  bool removeTransaction(Transaction transaction) {
    if(transaction.isDebt() && !transaction.isChecked) return false;

    switch(transaction.type){
      case "income":
        _incomes.remove(transaction);
        return true;
        break;
      case "expense":
        _expenses.remove(transaction);
        return true;
        break;
      case "debt":
        _debts.remove(transaction);
        return true;
        break;
      default:
        return false;
    }
  }

  void sellIncome(Transaction transaction, double value){
    receive(value);
    _incomes.remove(transaction);
  }

  Map<String, dynamic> fullData(){
    Map<String, dynamic> _data = {
      "$_name": {
        "job": _job,
        "salary": _salary,
        "balance": _balance,
        "expenses": {
          "initial": {},
          "current": {}
        },
        "debts": {
          "initial": {},
          "current": {}
        },
        "incomes": {},
        "stocks": {}
      }
    };

    _expenses.forEach((_t) {
      String _type = (_t.isDismissible || _t.title == "Filho") ? "current" : "initial";

      _data["$_name"]["expenses"][_type][_t.title] = _t.value;
    });

    _incomes.forEach((_t) {
      if(_t.isDismissible){
        _data["$_name"]["incomes"][_t.title] = _t.value;
      }
    });

    _debts.forEach((_t) {
      String _type = _t.isDismissible ? "current" : "initial";

      _data["$_name"]["debts"][_type][_t.title] = {
        "total": _t.total,
        "expenses": _t.value,
        "checked": _t.isChecked
      };
    });

    _stocks.forEach((_t) {
      _data["$_name"]["stocks"][_t.title] = _t.value;
    });

    return _data;
  }

  static final Map<String, dynamic> _charactersData = {
    "Alex": {
      "job": "Servidor público",
      "salary": 10000.0,
      "expenses": {
        "initial": {
          "Gastos Fixos": 3500.0,
          "Gastos Variáveis": 1700.0,
          "Impostos e Seguros": 500.0
        },
        "current": {}
      },
      "debts": {
        "initial": {
          "Cartão de crédito": { "total": 6000.0, "expenses": 1200.0 },
          "Diversos": { "total": 8000.0, "expenses": 1000.0 },
          "Financiamento": { "total": 60000.0, "expenses": 1200.0 }
        },
        "current": {}
      },
      "incomes": {}
    },
    "Márcia": {
      "job": "Advogada",
      "salary": 9000.0,
      "expenses": {
        "initial": {
          "Gastos Fixos": 2750.0,
          "Gastos Variáveis": 1000.0,
          "Impostos e Seguros": 1000.0
        },
        "current": {}
      },
      "debts": {
        "initial": {
          "Cartão de crédito": { "total": 6000.0, "expenses": 1200.0 },
          "Diversos": { "total": 3500.0, "expenses": 500.0 },
          "Financiamento": { "total": 45000.0, "expenses": 1800.0 }
        },
        "current": {}
      },
      "incomes": {}
    },
    "Bruno": {
      "job": "Microempresário",
      "salary": 7000.0,
      "expenses": {
        "initial": {
          "Gastos Fixos": 2300.0,
          "Gastos Variáveis": 1500.0,
          "Impostos e Seguros": 500.0
        },
        "current": {}
      },
      "debts": {
        "initial": {
          "Cartão de crédito": { "total": 4000.0, "expenses": 800.0 },
          "Diversos": { "total": 2000.0, "expenses": 300.0 },
          "Financiamento": { "total": 20000.0, "expenses": 400.0 }
        },
        "current": {}
      },
      "incomes": {}
    },
    "Eduardo": {
      "job": "Empresário",
      "salary": 30000.0,
      "expenses": {
        "initial": {
          "Gastos Fixos": 10000.0,
          "Gastos Variáveis": 8000.0,
          "Impostos e Seguros": 3400.0
        },
        "current": {}
      },
      "debts": {
        "initial": {
          "Cartão de crédito": { "total": 10000.0, "expenses": 2500.0 },
          "Diversos": { "total": 18000.0, "expenses": 2250.0 },
          "Financiamento": { "total": 100000.0, "expenses": 2000.0 }
        },
        "current": {}
      },
      "incomes": {}
    },
    "Jorge": {
      "job": "Motorista de Aplicativo",
      "salary": 3500.0,
      "expenses": {
        "initial": {
          "Gastos Fixos": 1000.0,
          "Gastos Variáveis": 350.0,
          "Impostos e Seguros": 300.0
        },
        "current": {}
      },
      "debts": {
        "initial": {
          "Cartão de crédito": { "total": 1500.0, "expenses": 300.0 },
          "Diversos": { "total": 500.0, "expenses": 90.0 },
          "Financiamento": { "total": 30000.0, "expenses": 600.0 }
        },
        "current": {}
      },
      "incomes": {}
    },
    "Leila": {
      "job": "Professora",
      "salary": 4500.0,
      "expenses": {
        "initial": {
          "Gastos Fixos": 1500.0,
          "Gastos Variáveis": 900.0,
          "Impostos e Seguros": 200.0
        },
        "current": {}
      },
      "debts": {
        "initial": {
          "Cartão de crédito": { "total": 2000.0, "expenses": 400.0 },
          "Diversos": { "total": 800.0, "expenses": 120.0 },
          "Financiamento": { "total": 10000.0, "expenses": 250.0 }
        },
        "current": {}
      },
      "incomes": {}
    }
  };
}