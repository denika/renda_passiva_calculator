import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

/*
 * MONEY METHODS
 */
String toMoney(double value){
  return NumberFormat.currency(
    symbol: "R\$",
    decimalDigits: 2,
  ).format(value).replaceFirst(".", ":").replaceAll(",", ".").replaceFirst(":", ",");
}

class MoneyText extends StatelessWidget {
  final double total;
  final Color color;
  final double fontSize;

  const MoneyText(this.total, {
    this.color = Colors.black,
    this.fontSize = 24.0
  });

  @override
  Widget build(BuildContext context) {
    return Text(toMoney(total),
      style: TextStyle(
          fontSize: fontSize,
          color: color
      ),
    );
  }
}

/*
* BASIC CONTAINERS
* */
class Box extends StatelessWidget {
  final double padding;
  final Color color;
  final Widget child;

  const Box({
    @required this.child,
    this.color = Colors.orange,
    this.padding = 10.0
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(padding),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: child
    );
  }
}

class BoxTitleText extends StatelessWidget {
  final String txt;
  final Color textColor;
  final FontWeight fontWeight;

  const BoxTitleText(this.txt, { this.textColor = Colors.white, this.fontWeight = FontWeight.w900 });

  @override
  Widget build(BuildContext context) {
    return Text(txt,
      style: TextStyle(
          fontSize: 24.0,
          color: textColor,
          fontWeight: fontWeight
      ),
    );
  }
}
