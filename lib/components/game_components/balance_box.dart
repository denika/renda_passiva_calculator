import 'package:flutter/material.dart';

import '../helpers.dart';

class BalanceBox extends StatelessWidget {
  final double total;

  const BalanceBox(this.total);

  @override
  Widget build(BuildContext context) {
    return Box(
        color: Colors.deepOrange,
        child: Row(
          children: <Widget>[
            Expanded(
              child: BoxTitleText("SALDO"),
            ),
            MoneyText(total)
          ],
        )
    );
  }
}