import 'package:flutter/material.dart';

import '../helpers.dart';

/*
* GENERIC TRANSACTIONS BOX
* */
class TransactionBox extends StatelessWidget {
  final Color color;
  final String title;
  final String actionText;
  final double total;
  final List<dynamic> _list;
  final Function itemBuilder;
  final Function actionOnPressed;

  TransactionBox(this._list, {
    this.title,
    this.actionText,
    this.color,
    this.total,
    this.itemBuilder,
    this.actionOnPressed
  });

  @override
  Widget build(BuildContext context) {
    return Box(
        padding: 2.0,
        color: color,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            TransactionBoxTitle(title, total: total),
            TransactionBoxList(
              listCount: _list.length,
              itemBuilder: itemBuilder,
            ),
          ],
        )
    );
  }
}

/*
* TRANSACTIONS BOX - TITLE
* */
class TransactionBoxTitle extends StatelessWidget {
  final String title;
  final double total;

  TransactionBoxTitle(this.title, {this.total});

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
        child: Row(
          children: <Widget>[
            Expanded(child: BoxTitleText(title)),
            (total == null ? Container() : MoneyText(total, color: Colors.white))
          ],
        )
    );
  }
}

/*
* TRANSACTIONS BOX - TRANSACTIONS LIST
* */
class TransactionBoxList extends StatefulWidget {
  final int listCount;
  final Function itemBuilder;

  TransactionBoxList({this.listCount = 0, this.itemBuilder});

  @override
  _TransactionBoxListState createState() => _TransactionBoxListState();
}

class _TransactionBoxListState extends State<TransactionBoxList> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(4.0), bottomLeft: Radius.circular(4.0))
        ),
        child: ListView.builder(
          physics: new NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          padding: EdgeInsets.symmetric(vertical: 10.0),
          itemCount: widget.listCount,
          itemBuilder: widget.itemBuilder,
        )
    );
  }
}
