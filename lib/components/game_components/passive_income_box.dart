import 'package:flutter/material.dart';

import '../helpers.dart';

class PassiveIncomeBox extends StatelessWidget {
  final double total;

  const PassiveIncomeBox(this.total);

  @override
  Widget build(BuildContext context) {
    return Box(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            BoxTitleText("RENDA PA\$\$IVA",
              textColor: Colors.deepOrange,
            ),
            MoneyText(total, fontSize: 30.0),
          ],
        )
    );
  }
}
