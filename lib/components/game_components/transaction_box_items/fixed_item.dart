import 'package:flutter/material.dart';
import 'package:renda_passiva_calculator/resources/transactions.dart';

import 'item_icon.dart';
import 'item_title.dart';

// -- BOX LIST ITEM - FIXED TRANSACTION TYPE
class FixedBoxListItem extends StatefulWidget {
  final Transaction transaction;

  FixedBoxListItem(this.transaction);

  @override
  _FixedBoxListItemState createState() => _FixedBoxListItemState();
}

class _FixedBoxListItemState extends State<FixedBoxListItem> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
        contentPadding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
        leading: BoxListItemIcon(widget.transaction.defaultIcon(),
            color: widget.transaction.defaultColor()
        ),
        title: BoxListItemTitle(
          title: widget.transaction.title,
          color: widget.transaction.titleColor(),
          value: widget.transaction.value,
        )
    );
  }
}