import 'package:flutter/material.dart';

import 'item_icon.dart';

class ActionBoxListItem extends StatefulWidget {
  final Widget title;
  final Color color;
  final IconData icon;
  final List<Map<String, dynamic>> actions;

  ActionBoxListItem({this.title, this.icon, this.color, this.actions});

  @override
  _ActionBoxListItemState createState() => _ActionBoxListItemState();
}

class _ActionBoxListItemState extends State<ActionBoxListItem> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: Row(
        children: <Widget>[
          BoxListItemIcon(widget.icon, color: widget.color),
          Expanded(
            child: Container(
                padding: EdgeInsets.only(left: 10.0),
                child: widget.title
            ),
          ),
          _actions()
        ],
      ),
    );
  }

  Widget _actions(){
    return Row(
      children: widget.actions.map((action){
        return Container(
          width: (widget.actions.length > 1 ? 70.0 : 100.0),
          padding: EdgeInsets.only(left: 10.0),
          child: RaisedButton(
            child: action["title"],
            color: widget.color,
            onPressed: action["onPressed"],
          ),
        );
      }).toList()
    );
  }
}
