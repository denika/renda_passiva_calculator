import 'package:flutter/material.dart';

import '../../helpers.dart';

class BoxListItemTitle extends StatefulWidget {
  final String title;
  final Color color;
  final double value;

  BoxListItemTitle({this.title, this.color, this.value});

  @override
  _BoxListItemTitleState createState() => _BoxListItemTitleState();
}

class _BoxListItemTitleState extends State<BoxListItemTitle> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(widget.title,
          style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
              color: widget.color
          ),
        ),
        MoneyText(
            widget.value,
            fontSize: 16.0,
            color: Colors.black87
        )
      ],
    );
  }
}