import 'package:flutter/material.dart';

class BoxListItemIcon extends StatefulWidget {
  final IconData icon;
  final bool isChecked;
  final Color color;

  BoxListItemIcon(this.icon, {this.isChecked = false, this.color = Colors.orange});

  @override
  _BoxListItemIconState createState() => _BoxListItemIconState();
}

class _BoxListItemIconState extends State<BoxListItemIcon> {
  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      child: Icon(widget.isChecked ? Icons.check : widget.icon,
          color: Colors.white
      ),
      backgroundColor: widget.color,
    );
  }
}
