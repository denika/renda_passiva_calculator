import 'package:flutter/material.dart';
import 'package:renda_passiva_calculator/resources/transactions.dart';

import 'item_icon.dart';
import 'item_title.dart';

class CheckableBoxListItem extends StatefulWidget {
  final Transaction transaction;
  final double value;
  final Color color;
  final Function onChanged;

  const CheckableBoxListItem(this.transaction, {this.value, this.color, this.onChanged});

  @override
  _CheckableBoxListItemState createState() => _CheckableBoxListItemState();
}

class _CheckableBoxListItemState extends State<CheckableBoxListItem> {
  @override
  Widget build(BuildContext context) {
    return ListTileTheme(
        contentPadding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
        child: CheckboxListTile(
          title: BoxListItemTitle(
            title: widget.transaction.title,
            color: widget.transaction.titleColor(widget.color),
            value: widget.value,
          ),
          value: widget.transaction.isChecked,
          secondary: BoxListItemIcon(widget.transaction.defaultIcon(),
              color: widget.color
          ),
          activeColor:  widget.color,
          onChanged: widget.onChanged,
        )
    );
  }
}
