import 'package:flutter/material.dart';
import 'package:renda_passiva_calculator/resources/transactions.dart';

import 'fixed_item.dart';

class DismissibleBoxListItem extends StatefulWidget {
  final Transaction transaction;
  final Widget child;
  final Function onDismissed;

  DismissibleBoxListItem(this.transaction, {this.child, this.onDismissed});

  @override
  _DismissibleBoxListItemState createState() => _DismissibleBoxListItemState();
}

class _DismissibleBoxListItemState extends State<DismissibleBoxListItem> {
  @override
  Widget build(BuildContext context) {
    return Dismissible(
        confirmDismiss: _confirmDismiss,
        background: Container(
            color: Colors.deepOrange,
            child: Align(
                alignment: Alignment(-0.9, 0.0),
                child: Icon(Icons.monetization_on,
                    color: Colors.white
                )
            )
        ),
        direction: DismissDirection.startToEnd,
        key: Key(DateTime.now().millisecondsSinceEpoch.toString()),
        child: widget.child,
        onDismissed: widget.onDismissed
    );
  }

  Future<bool> _confirmDismiss(direction) async {
    if(widget.transaction.isDebt()) return widget.transaction.isChecked;

    return true;
  }
}
