import 'package:flutter/material.dart';
import 'package:renda_passiva_calculator/resources/transactions.dart';

import '../../constants.dart';
import 'dialog_components.dart';

class IncomeTransactionDialog extends StatefulWidget {
  @override
  _IncomeTransactionDialogState createState() => _IncomeTransactionDialogState();
}

class _IncomeTransactionDialogState extends State<IncomeTransactionDialog> {
  Transaction _t = new Transaction("income", title: 'Aumento');

  final List<String> _incomeTypes = ["Aumento", "Negócios", "Imóvel", "Investimento"];

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;

  TextEditingController valueController = TextEditingController();
  TextEditingController priceController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Nova receita",
        style: TextStyle(
          color: Colors.green,
        )
      ),
      content: Form(
        key: _formKey,
        autovalidate: _autoValidate,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            _dropdownSelector(),
            _buyPriceField(),
            _valueField(),
          ],
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text("Cancelar", style: TextStyle(color: Colors.grey)),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        FlatButton(
          child: Text("OK", style: TextStyle(color: Colors.green)),
          onPressed: (){
            if(_formKey.currentState.validate()){
              Navigator.pop(context, _t);
            }

            setState(() {
              _autoValidate = true;
            });
          },
        ),
      ],
    );
  }

  Widget _dropdownSelector(){
    return DialogDropdownSelector(
      value: _t.title,
      optionsValues: _incomeTypes,
      color: THEME_COLORS["Receitas"],
      onChanged: (String val){
        setState(() {
          setState(() {
            _t.title = val;
          });
        });
      },
    );
  }

  Widget _buyPriceField(){
    if(_t.title == null) {
      return Container(height: 0.0);
    } else if(_t.title == 'Aumento') {
      return Container(height: 0.0);
    } else {
      return _priceField(
        label: "Valor de compra",
        controller: priceController,
        onChanged: (buyPrice) {
          setState(() {
            _t.total = double.parse(buyPrice);
          });
        },
      );
    }

  }

  Widget _valueField(){
    String _label = (_t.title == 'Aumento' ? 'Valor do Aumento' : 'Retorno mensal');

    return _priceField(
      label: _label,
      controller: valueController,
      onChanged: (income){
        setState(() {
          _t.value = double.parse(income);
        });
      }
    );
  }

  Widget _priceField({String label, Function onChanged, TextEditingController controller}){
    return DialogTextField(
      title: label,
      color: Colors.green,
      controller: controller,
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      validator: (value){
        if(value.isEmpty){
          return "* Campo obrigatório";
        }
        return null;
      },
      onChanged: onChanged,
      prefix: "R\$"
    );
  }
}
