import 'package:flutter/material.dart';
import 'package:renda_passiva_calculator/resources/transactions.dart';

import '../../constants.dart';
import 'dialog_components.dart';

class StockTransactionDialog extends StatefulWidget {
  final String title;
  final Transaction stock;

  StockTransactionDialog(this.title, {this.stock});

  @override
  _StockTransactionDialogState createState() => _StockTransactionDialogState();
}

class _StockTransactionDialogState extends State<StockTransactionDialog> {
  final List<String> _stocks = ["APPL3", "VAL3", "BB4S", "AMZ4"];
  Transaction _t;
  int _maxQuantity;

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;

  final quantityController = TextEditingController();
  final priceController = TextEditingController();
  final titleController = TextEditingController();

  @override
  void initState() {
    super.initState();

    _t = Transaction("stock",
        title: widget.stock?.title,
        isDismissible: true,
    );
    priceController.text = "25.0";
    _t.total = 25.0;
    _maxQuantity = widget.stock?.value?.floor() ?? 999999999;
    titleController.text = _t.title;

    print(_t);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(widget.title,
        style: TextStyle(
          color: Colors.purple,
        ),
      ),
      content: Form(
        key: _formKey,
        autovalidate: _autoValidate,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            (widget.stock == null ? _dropdownSelector() : _fixedTitleField()),
            _quantityField(),
            _priceField(),
          ],
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text("Cancelar", style: TextStyle(color: Colors.grey)),
          onPressed: (){
            _t.value = null;
            Navigator.pop(context, _t);
          },
        ),
        FlatButton(
          child: Text("OK", style: TextStyle(color: Colors.green)),
          onPressed: (){
            if(_formKey.currentState.validate()){
              Navigator.pop(context, _t);
            }

            setState(() {
              _autoValidate = true;
            });
          },
        ),
      ],
    );
  }

  Widget _dropdownSelector(){
    return DialogDropdownSelector(
      value: _t.title,
      optionsValues: _stocks,
      color: THEME_COLORS["Ativos"],
      onChanged: (String val){
        setState(() {
          _t.title = val;
        });
      },
    );
  }

  Widget _fixedTitleField(){
    return DialogTextField(
        title: "Ação",
        color: THEME_COLORS["Ativos"],
        controller: titleController,
        validator: (value){ return null; },
        onChanged: (value){},
        isEnabled: false
    );
  }

  Widget _quantityField(){
    return DialogTextField(
      title: "Quantidade",
      color: THEME_COLORS["Ativos"],
      controller: quantityController,
      keyboardType: TextInputType.number,
      validator: (value){
        if(value.isEmpty){ return "* Campo obrigatório"; }
        if(int.parse(value) > _maxQuantity){ return "* Valor máximo: $_maxQuantity"; }
        return null;
      },
      onChanged: (value){
        _t.value = double.parse(value);
      }
    );
  }

  Widget _priceField(){
    return DialogTextField(
        title: "Valor da ação",
        color: THEME_COLORS["Ativos"],
        controller: priceController,
        keyboardType: TextInputType.number,
        validator: (value){
          if(value.isEmpty){
            return "* Campo obrigatório";
          }
          return null;
        },
        onChanged: (value){
          _t.total = double.parse(value);
        },
        prefix: "R\$"
    );
  }
}
