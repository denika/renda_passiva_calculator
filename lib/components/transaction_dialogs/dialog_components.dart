import 'package:flutter/material.dart';

class DialogTextField extends StatelessWidget {
  final String title;
  final Color color;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final Function validator;
  final Function onChanged;
  final String prefix;
  final bool isEnabled;

  DialogTextField({
    this.title,
    this.color = Colors.orange,
    this.controller,
    this.keyboardType = TextInputType.text,
    this.validator,
    this.onChanged,
    this.prefix = "",
    this.isEnabled = true
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(
          labelText: title,
          labelStyle: TextStyle(color: color),
          border: UnderlineInputBorder(borderSide: BorderSide(color: color)),
          focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: color)),
          enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: color)),
          prefixText: prefix,
          prefixStyle: TextStyle(color: color),
          hintStyle: TextStyle(color: Colors.black),
      ),
      keyboardType: keyboardType,
      style: TextStyle(
          color: Colors.black
      ),
      validator: validator,
      onChanged: onChanged,
      enabled: isEnabled,
    );
  }
}

class DialogDropdownSelector extends StatefulWidget {
  final String value;
  final List<String> optionsValues;
  final Function onChanged;
  final Color color;

  DialogDropdownSelector({
    @required this.optionsValues,
    @required this.value,
    @required this.onChanged,
    @required this.color
  });

  @override
  _DialogDropdownSelectorState createState() => _DialogDropdownSelectorState();
}

class _DialogDropdownSelectorState extends State<DialogDropdownSelector> {
  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
        hint: Text("Selecione...", style: TextStyle(color: Colors.black),),
        value: widget.value,
        isExpanded: true,
        underline: Container(
            height: 1,
            color: widget.color
        ),
        items: widget.optionsValues.map((v){
          return DropdownMenuItem(
            child: Text(v),
            value: v,
          );
        }).toList(),
        onChanged: widget.onChanged
    );
  }
}
