import 'package:flutter/material.dart';

import 'package:renda_passiva_calculator/components/transaction_dialogs/dialog_components.dart';

class BalanceChangeDialog extends StatefulWidget {
  final String _title;

  BalanceChangeDialog(this._title);

  @override
  _BalanceChangeDialogState createState() => _BalanceChangeDialogState(_title);
}

class _BalanceChangeDialogState extends State<BalanceChangeDialog> {
  final String _title;

  _BalanceChangeDialogState(this._title);

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;

  TextEditingController valueController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(_title,
        style: TextStyle(
          color: Colors.orange
        ),
      ),
      content: Form(
        key: _formKey,
        autovalidate: _autoValidate,
        child: DialogTextField(
          title: "Valor",
          controller: valueController,
          keyboardType: TextInputType.numberWithOptions(signed: true, decimal: true),
          validator: (value){
              if(value.isEmpty){
                return "* Campo obrigatório";
              }
            return null;
          },
          onChanged: (price){},
          prefix: "R\$"
        )
      ),
      actions: <Widget>[
        FlatButton(
          child: Text("Cancelar", style: TextStyle(color: Colors.grey)),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        FlatButton(
          child: Text("OK", style: TextStyle(color: Colors.orange)),
          onPressed: (){
            if(_formKey.currentState.validate()){
              Navigator.pop(context, double.parse(valueController.text));
            }

            setState(() {
              _autoValidate = true;
            });
          },
        ),
      ],
    );
  }
}
