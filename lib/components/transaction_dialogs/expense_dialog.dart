import 'package:flutter/material.dart';
import 'package:renda_passiva_calculator/resources/transactions.dart';

import '../../constants.dart';
import 'dialog_components.dart';

class ExpenseTransactionDialog extends StatefulWidget {
  final double _income;

  ExpenseTransactionDialog(this._income);
  
  @override
  _ExpenseTransactionDialogState createState() => _ExpenseTransactionDialogState();
}

class _ExpenseTransactionDialogState extends State<ExpenseTransactionDialog> {
  Transaction _t = new Transaction("expense");

  final titleController = TextEditingController();
  final valueController = TextEditingController();

  String selectorValue = "Outros";
  List<String> _expenseTypes = ["Outros", "Filho"];

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Novo gasto",
          style: TextStyle(
            color: Colors.red,
          )
      ),
      content: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _dropdownSelector(),
              _titleField(),
              _valueField(),
            ],
          ),
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text("Cancelar", style: TextStyle(color: Colors.grey)),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        FlatButton(
          child: Text("OK", style: TextStyle(color: Colors.red)),
          onPressed: (){
            if(_formKey.currentState.validate()){
              Navigator.pop(context, _t);
            }

            setState(() {
              _autoValidate = true;
            });
          },
        ),
      ],
    );
  }

  Widget _dropdownSelector(){
    return DialogDropdownSelector(
      value: selectorValue,
      optionsValues: _expenseTypes,
      color: THEME_COLORS["Gastos"],
      onChanged: (String val){
        setState(() {
          selectorValue = val;

          if(selectorValue == "Filho"){
            print(widget._income);
            titleController.text = val;
            valueController.text = (widget._income/10).toStringAsFixed(2);

            _t.title = val;
            _t.isDismissible = false;
            _t.value = double.parse(valueController.text);
          }
          else {
            titleController.text = "";
            valueController.text = "";

            _t.title = null;
            _t.value = null;
            _t.isDismissible = true;
          }
        });
      },
    );
  }

  Widget _titleField(){
    return DialogTextField(
        title: "Título",
        color: THEME_COLORS["Gastos"],
        controller: titleController,
        keyboardType: TextInputType.text,
        validator: (value){
          if(value.isEmpty){
            return "* Campo obrigatório";
          }
          return null;
        },
        onChanged: (value){
          _t.title = value;
        }
    );
  }

  Widget _valueField(){
    return DialogTextField(
        title: "Valor",
        color: THEME_COLORS["Gastos"],
        controller: valueController,
        keyboardType: TextInputType.numberWithOptions(decimal: true),
        validator: (value){
          if(value.isEmpty){
            return "* Campo obrigatório";
          }
          return null;
        },
        onChanged: (value){
          _t.value = double.parse(value);
        },
        prefix: "R\$",
        isEnabled: (selectorValue != "Filho")
    );
  }
}
