import 'package:flutter/material.dart';
import 'package:renda_passiva_calculator/resources/transactions.dart';

import 'dialog_components.dart';

class DebtTransactionDialog extends StatefulWidget {
  @override
  _DebtTransactionDialogState createState() => _DebtTransactionDialogState();
}

class _DebtTransactionDialogState extends State<DebtTransactionDialog> {
  Transaction _t = new Transaction("debt");

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;

  final titleController = TextEditingController();
  final totalController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Novo financiamento",
          style: TextStyle(
            color: Colors.cyan,
          )
      ),
      content: Form(
        key: _formKey,
        autovalidate: _autoValidate,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            _titleField(),
            _totalField(),
          ],
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text("Cancelar", style: TextStyle(color: Colors.grey)),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        FlatButton(
          child: Text("OK", style: TextStyle(color: Colors.cyan)),
          onPressed: (){
            if(_formKey.currentState.validate()){
              Navigator.pop(context, _t);
            }

            setState(() {
              _autoValidate = true;
            });
          },
        ),
      ],
    );
  }

  Widget _titleField(){
    return DialogTextField(
        title: "Título",
        color: Colors.cyan,
        controller: titleController,
        keyboardType: TextInputType.text,
        validator: (value){
          if(value.isEmpty){
            return "* Campo obrigatório";
          }
          return null;
        },
        onChanged: (value){
          _t.title = value;
        }
    );
  }

  Widget _totalField(){
    return DialogTextField(
        title: "Valor do empréstimo",
        color: Colors.cyan,
        controller: totalController,
        keyboardType: TextInputType.numberWithOptions(decimal: true),
        validator: (value){
          if(value.isEmpty){
            return "* Campo obrigatório";
          }
          return null;
        },
        onChanged: (value){
          _t.total = double.parse(value);
          _t.value = _t.total / 10;
        },
        prefix: "R\$"
    );
  }
}
