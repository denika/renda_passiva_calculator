import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

import '../constants.dart';

class FloatingButtonAction extends StatelessWidget {
  final Map<String, Function> _actions;

  FloatingButtonAction(this._actions);

  @override
  Widget build(BuildContext context) {
    return SpeedDial(
      child: Icon(Icons.attach_money),
      backgroundColor: Colors.orange,
      overlayOpacity: 0.7,
      overlayColor: Colors.black,
      children:
        _actions.entries.map((_action) {
          return _floatingButtonAction(
            title: _action.key,
            onTap: _action.value,
          );
        }).toList()
    );
  }

  static const Map<String, String> ACTION_TITLES = {
    "Receitas": "Nova Receita",
    "Gastos": "Novo Gasto Fixo",
    "Dívidas": "Novo Empréstimo",
    "Pagar": "PAGAR!",
    "Receber": "RECEBER!",
    "Ativos": "Comprar ação"
  };

  SpeedDialChild _floatingButtonAction({String title, Function onTap}){
    return SpeedDialChild(
        child: Icon(THEME_ICONS[title], color: THEME_COLORS[title],),
        backgroundColor: Colors.white,
        label: ACTION_TITLES[title],
        labelStyle: TextStyle(fontSize: 14.0),
        onTap: onTap
    );
  }
}
