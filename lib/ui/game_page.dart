import 'package:flutter/material.dart';
import 'package:renda_passiva_calculator/components/game_components/balance_box.dart';
import 'package:renda_passiva_calculator/components/game_components/passive_income_box.dart';
import 'package:renda_passiva_calculator/components/game_components/transaction_box.dart';
import 'package:renda_passiva_calculator/components/game_components/transaction_box_items/action_item.dart';
import 'package:renda_passiva_calculator/components/game_components/transaction_box_items/checkable_item.dart';
import 'package:renda_passiva_calculator/components/game_components/transaction_box_items/dismissible_item.dart';
import 'package:renda_passiva_calculator/components/game_components/transaction_box_items/fixed_item.dart';
import 'package:renda_passiva_calculator/components/helpers.dart';
import 'package:renda_passiva_calculator/components/transaction_actions.dart';
import 'package:renda_passiva_calculator/components/transaction_dialogs/balance_change_dialog.dart';
import 'package:renda_passiva_calculator/components/transaction_dialogs/stock_dialog.dart';
import 'package:renda_passiva_calculator/io/file.dart';
import 'package:renda_passiva_calculator/resources/transactions.dart';
import 'package:toast/toast.dart';

import 'package:renda_passiva_calculator/components/transaction_dialogs/debt_dialog.dart';
import 'package:renda_passiva_calculator/components/transaction_dialogs/expense_dialog.dart';
import 'package:renda_passiva_calculator/components/transaction_dialogs/income_dialog.dart';
import 'package:renda_passiva_calculator/resources/character.dart';

import '../constants.dart';

class GamePage extends StatefulWidget {
  final Character character;

  GamePage({this.character});

  @override
  _GamePageState createState(){
    return _GamePageState(character);
  }
}

class _GamePageState extends State<GamePage> {
  final Character _char;
  bool _gameStated = false;

  _GamePageState(this._char);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
            title: Text("Calculadora Renda Passiva"),
            backgroundColor: Colors.orange,
          ),
          body: SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                _charData(),
                Divider(),
                _transactionsBox(_char.incomes, "Receitas", _char.totalIncomes()),
                Divider(),
                _transactionsBox(_char.expenses + _char.debts, "Gastos", _char.totalExpenses()),
                Divider(),
                _transactionsBox(_char.debts, "Dívidas"),
                Divider(),
                _transactionsBox(_char.stocks, "Ativos"),
                Divider(),
                _transactionsBox([_char.payment()], "Pagamento"),
                Divider(),
                BalanceBox(_char.balance),
                Divider(),
                PassiveIncomeBox(_char.passiveIncome()),
                Divider(height: 30.0,),
              ],
            ),
          ),
          floatingActionButton: _floatingButton(),
        ),
        onWillPop: (){
          if(_gameStated){
            Navigator.pop(context, _char);
          } else {
            Navigator.pop(context);
          }

          return Future.value(true);
        }
    );
  }

  /*
   * CHARACTER DATA
   */
  Widget _charData(){
    return Row(
      children: <Widget>[
        CircleAvatar(
          backgroundColor: Colors.white,
          child: Icon(Icons.person, color: Colors.orange,),
        ),
        Container(
          padding: EdgeInsets.only(left: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(_char.name,
                  style: TextStyle(
                      fontSize: 24.0,
                      fontWeight: FontWeight.w900,
                      color: Colors.orange
                  )
              ),
              Text(_char.job,
                  style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.white
                  )
              ),
            ],
          ),
        ),
      ],
    );
  }

  /*
   * TRANSACTION BOXES
   */
  Widget _transactionsBox(list, title, [total]){
    return TransactionBox(list,
      title: title,
      color: THEME_COLORS[title],
      total: total,
      itemBuilder: (context, index){
        return _listItem(list[index], title);
      },
    );
  }

  Widget _listItem(item, title){
    if(title == "Pagamento") return _paymentActionBoxListItem(item);

    if(item.isDismissible) return _dismissibleBoxListItem(item, title);

    if(item.isDebt()) return _checkableBoxListItem(item, title);

    return FixedBoxListItem(item);
  }

  Widget _paymentActionBoxListItem(double payment){
    return ActionBoxListItem(
        title: MoneyText(payment),
        icon: Icons.attach_money,
        color: Colors.orange,
        actions: <Map<String, dynamic>>[
          {
            "title": Text("PAGAR!"),
            "onPressed": (){
              setState(() {
                _char.receivePayment();
              });
              _saveGame();
            }
          }
        ]
    );
  }

  Widget _dismissibleBoxListItem(Transaction item, String title){
    Widget _child;

    if(item.isDebt()) { _child = _checkableBoxListItem(item, title); }
    else if(item.isStock()){ _child = _stockActionBoxListItem(item); }
    else { _child = FixedBoxListItem(item); }

    return DismissibleBoxListItem(item,
        child: _child,
        onDismissed: (direction) {
          if(item.isIncome() && item.total != null){
            _showNewTransactionDialog(
                transactionDialog: BalanceChangeDialog("Venda de Ativo"),
                callback: (value){
                  setState(() {
                    _char.sellIncome(item, value);
                  });

                  Toast.show("Valor recebido!", context);
                  _saveGame();
                }
            );
          } else if(item.isStock()){
            _showNewTransactionDialog(
              transactionDialog: StockTransactionDialog("Venda de ações (${item.value.floor()})",
                stock: item,
              ),
              callback: (transaction){
                if(_char.sellStock(transaction)) {
                  Toast.show("Ações vendidas!", context);
                }
              }
            );
          } else {
            setState(() {
              _char.removeTransaction(item);
            });
            _saveGame();
          }
        }
    );
  }

  Widget _stockActionBoxListItem(Transaction item){
    if(item.value == 0){
      return null;
    }

    return ActionBoxListItem(
        title: Text(item.value.floor().toString(),
          style: TextStyle(
              fontSize: 18.0
          ),
        ),
        icon: THEME_ICONS[item.type],
        color: THEME_COLORS[item.title],
        actions: <Map<String, dynamic>>[
          {
            "title": Text("x 2"),
            "onPressed": (){
              setState(() {
                item.doubleValue();
              });
              _saveGame();
              Toast.show("Dobrou as ações ${item.title}", context);
            }
          },
          {
            "title": Text("/ 2"),
            "onPressed": (){
              setState(() {
                item.halveValue();
              });
              _saveGame();
              Toast.show("Perdeu metade das ações ${item.title}", context);
            }
          }
        ]
    );
  }

  Widget _checkableBoxListItem(Transaction item, String title){
    return CheckableBoxListItem(item,
        value: (title == "Dívidas" ? item.total : item.value),
        color: THEME_COLORS[title],
        onChanged: (checked){
          setState((){
            if(checked){
              if(_char.pay(item.total)){
                item.isChecked = true;
                _saveGame();
              } else {
                Toast.show("Saldo insuficiente!", context);
              }
            } else {
              _char.receive(item.total);
              item.isChecked = false;
              _saveGame();
            }
          });
        }
    );
  }

  /*
   * FLOATING BUTTONS + TRANSACTION DIALOGS
   */
  Widget _floatingButton(){
    return FloatingButtonAction(
      {
        "Receitas": (){
          _showNewTransactionDialog(
              transactionDialog: IncomeTransactionDialog(),
              callback: (transaction){
                if(!_char.addIncome(transaction)){
                  Toast.show("Não foi possível adicionar nova receita :(", context);
                }
              }
          );
        },
        "Gastos": (){
          _showNewTransactionDialog(
              transactionDialog: ExpenseTransactionDialog(_char.salary),
              callback: (transaction){
                setState(() {
                  _char.addExpense(transaction);
                });

              }
          );
        },
        "Dívidas": (){
          _showNewTransactionDialog(
            transactionDialog: DebtTransactionDialog(),
            callback: (transaction){
              _char.addDebt(transaction);
            }
          );
        },
        "Ativos": (){
          _showNewTransactionDialog(
              transactionDialog: StockTransactionDialog("Compra de Ações"),
              callback: (transaction){
                if(transaction.value != null && transaction.value > 0) {
                  if (_char.buyStock(transaction)) {
                    Toast.show("Ações compradas!", context);
                  } else {
                    Toast.show("Não foi possível comprar as ações :(", context);
                  }
                }
              }
          );
        },
        "Pagar": (){
          _showNewTransactionDialog(
            transactionDialog: BalanceChangeDialog("Pagamento"),
            callback: (value){
              if(_char.pay(value)){
                Toast.show("Pagamento realizado!", context);
              }
            }
          );
        },
        "Receber": (){
          _showNewTransactionDialog(
            transactionDialog: BalanceChangeDialog("Recebimento"),
            callback: (value){
              _char.receive(value);
              Toast.show("Valor recebido!", context);
            }
          );
        },
      }
    );
  }

  void _showNewTransactionDialog({Widget transactionDialog, Function callback}) async {
    showDialog(
        context: context,
        builder: (context){
          return transactionDialog;
        }
    ).then((transaction){
      if(transaction != null){
        setState(() {
          callback(transaction);
        });
        _saveGame();
      }
    });
  }

  /*
   * SAVE GAME
   */
  void _saveGame(){
    saveData(_char.fullData());
    _gameStated = true;
  }
}
