import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import 'package:renda_passiva_calculator/io/file.dart';
import 'package:renda_passiva_calculator/resources/character.dart';

import '../constants.dart';
import 'game_page.dart';

Future<Map> getData() async {
  String gameData = await readData();
  return json.decode(gameData);
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String _newCharName;
  Character _char;
  Character _loadedChar;

  @override
  void initState() {
    super.initState();

    getData().then((data){
      setState(() {
        _loadedChar = Character(data.keys.toList()[0], baseData: data);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text("Calculadora Renda Passiva"),
        backgroundColor: Colors.orange,
      ),
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Icon(Icons.monetization_on, size: 150.0, color: Colors.orange),
            Divider(),
            _buildLoadGame(),
            Divider(),
            Container(
              padding: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                  border: Border.all(width: 1.0, color: Colors.orange),
                  borderRadius: BorderRadius.all(Radius.circular(5.0))
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Text("Novo jogo", style: TextStyle(color: Colors.orange),),
                  Divider(),
                  DropdownButton<String>(
                      hint: Text("Selecione o personagem", style: TextStyle(color: Colors.white),),
                      value: _newCharName,
                      isExpanded: true,
                      style: TextStyle(color: Colors.white, fontSize: 16.0),
                      dropdownColor: Colors.black54,
                      underline: Container(
                        height: 1,
                        color: Colors.orange,
                      ),
                      items: CHARACTERS.map((v){
                        return DropdownMenuItem(
                          child: Text(v),
                          value: v,
                        );
                      }).toList(),
                      onChanged: (selectedChar){
                        setState(() {
                          _newCharName = selectedChar;
                        });
                      }
                  ),
                  Divider(),
                  Container(
                      height: 50.0,
                      child: RaisedButton(
                        child: Text("Começar jogo", style: TextStyle(fontSize: 18.0),),
                        color: Colors.orange,
                        onPressed: (){
                          if(_newCharName == null){
                            Toast.show("Selecione um personagem!", context);
                          } else {
                            _char = Character(_newCharName);
                            _showGamePage();
                          }
                        },
                      )
                  )
                ],
              ),
            ),
          ],
        ),
      )
    );
  }

  Widget _buildLoadGame(){
    if(_loadedChar == null) return Container();

    String _charName = _loadedChar.name;

    return Container(
        height: 50.0,
        child: RaisedButton(
          child: Text("Continuar jogo ($_charName)",
              style: TextStyle(fontSize: 18.0)
          ),
          color: Colors.orange,
          onPressed: (){
            _char = _loadedChar;
            _showGamePage();
          },
        )
    );
  }

  void _showGamePage() async {
    if(_char == null){
      Toast.show("Não foi possível carregar jogo!", context);
      return;
    }

    final _recChar = await Navigator.push(context,
        MaterialPageRoute(builder: (context) => GamePage(character: _char))
    );

    if(_recChar != null){
      setState(() {
        _loadedChar = _recChar;
      });
    }
  }
}
